import React from "react";
import { StyleSheet, Text, View } from "react-native";

import Flag from "../../components/Flag";

export default function Root() {
  return (
    <View style={styles.container}>
      <Text>Open up App.tsx to start working on your app!!!!!!!!</Text>
      <Flag country="cl" size="sm" />
      <Flag country="br" size="md" />
      <Flag country="ar" size="lg" />
      <Flag country="ng" size="xl" />
      <Flag size="xxl" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
