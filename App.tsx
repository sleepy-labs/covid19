import React from "react";
import Root from "./src/containers/Root";

export default function App() {
  return <Root />;
}
