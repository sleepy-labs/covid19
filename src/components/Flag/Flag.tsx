import React from "react";
import { Image, StyleSheet } from "react-native";
import PropTypes, { InferProps } from "prop-types";
import countries from "./countries";

export default function Flag({
  country,
  size,
}: InferProps<typeof Flag.propTypes>) {
  return (
    <Image
      source={countries[country as keyof typeof countries]}
      style={styles[size as keyof typeof styles]}
    />
  );
}

const styles = StyleSheet.create({
  sm: {
    width: 24,
    height: 15,
  },
  md: {
    width: 48,
    height: 30,
  },
  lg: {
    width: 72,
    height: 45,
  },
  xl: {
    width: 96,
    height: 60,
  },
  xxl: {
    width: 120,
    height: 75,
  },
});

Flag.propTypes = {
  country: PropTypes.string.isRequired,
  size: PropTypes.oneOf(["sm", "md", "lg", "xl", "xxl"]).isRequired,
};

Flag.defaultProps = {
  country: "unknow",
  size: "md",
};
